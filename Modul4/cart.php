<?php
session_start();
if(!(isset($_SESSION["login"]))){
    echo "<script>window.location.href='Home.php'</script>";
        exit;
}
include_once("config.php");

$result = mysqli_query($conn, "SELECT*FROM cart_table");
?>

<!DOCTYPE html>
<html>

<head>
    <title>Cart</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="style.css" rel="stylesheet" type="text/css">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
</head>

<body style="background : url('https://www.fg-a.com/wallpapers/white-marble-1-2018.jpg'); background-size:cover;">

<nav>
        <a class="navbar-brand" href="Home.php"> <br>
            <img src="ead.png" height="40px" style="margin-left:20px;">
        </a>
        <div style="float: right; margin-right: 40px;">
            <table>
                <tr>
                    <td><br><a href="cart.php"><img src="cart.png" alt="Cart" width="20px"></a></td>
                    <td>
                        <div class="bs-example" style="padding: 5px;">
                            <br>&ensp;<a href="#" class="dropdown-toggle" data-toggle="dropdown"><?=$_SESSION["user"]?></a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a href="profil.php" class="dropdown-item">Profile</a>
                                <div class="dropdown-divider"></div>
                                <a href="logout.php" class="dropdown-item">Logout</a>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </nav>

  
<hr> <br>
<table rules = "rows" style="width: 60%;" align="center">
        <tr>
            <th>No</th>
            <th>Product</th>
            <th>Price</th>
            <th> </th>
        </tr>
    

    <?php
    $id = $_SESSION["iduser"];
    $data = mysqli_query($conn, "SELECT * FROM cart_table WHERE user_id = '$id'");
    $no=1;
    $price=0;
        while($row = mysqli_fetch_array($data)){
            echo "<tr>";
            echo "<td>$no</td>";
            echo "<td>"; 
            echo $row['product'];
            echo "</td>";
            echo "<td>";
            echo $row['price'];
            echo "</td>";
            echo "<td><a href='delete.php?id=".$row['id']." 'name='delete' class='btn btn-danger' data-dismiss='modal'>&times</a></td>";
            $no=$no+1;
            $price=$price+$row['price'];
            echo "</tr>";
            
        }
        
  ?>

    <tr>
        <td colspan="2"><b>Total</b></td>
        <td colspan="2"><b><?=$price?></b></td>
    </tr>
</table>


<br>

<h6 style="text-align:center;">@ EAD STORE </h6>


</body>

</html>