<?php
session_start();
?>

<!DOCTYPE html>

<html>

<head>
  <title>Home</title>
  <link href="style.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
  </script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
  </script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
  </script>
  
</head>
<body style="background : url('https://www.fg-a.com/wallpapers/white-marble-1-2018.jpg'); background-size:cover;">
 
<?php
if(!isset($_SESSION["user"])){
  echo '<a class="navbar-brand" href="Home.php"> <img src = "ead.png" width="170px" height ="50px" style="margin-left:15px;"> </a>

  <div style="float: right;">
    <ul class="nav navbar-right" style="font-size:16px; margin-right: 20px;">
      <li data-toggle="modal" data-target="#login"><a href="#"><br>Login</a></li>
      <li data-toggle="modal" data-target="#regis"><a href="#"><br>Register</a></li>
    </ul> 
  </div> ';

}else{
  echo '<nav class="navbar navbar-light bg-light">
    <a class="navbar-brand" href="Home.php"> <img src="ead.png" height="35px"> </a>

    <div style="float: right; margin-right: 40px;">
      <table>
        <tr>
        <td><a href="cart.php"><img src="cart.png" alt="Cart" width="20px"></a></td>

        <td> <div class="bs-example" style="padding: 5px;">
               &ensp;<a href="#" class="dropdown-toggle" data-toggle="dropdown">';
               echo $_SESSION["user"];
               echo '</a>
             
            <div class="dropdown-menu dropdown-menu-right">
               <a href="profil.php" class="dropdown-item">Profile</a>
             
            <div class="dropdown-divider"></div>
                <a href="logout.php" class="dropdown-item">Logout</a>
            </div>
            </div>
        </td>
        </tr>
      </table>


    </div>
    </nav>';
}
?>


<br><br>
  <div id="login" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      
      <div class="modal-header">
        <h3> Login </h3>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <div class="modal-body">
        <form action = "crud.php" method="POST" >
          <label>Email Address</label>
          <input type="email" id="email" name="email" placeholder="Enter email" required>
          <label>Password</label>
          <input type="password" id="pass" name="pass" placeholder="Password" required>
      </div>

      <div class="klik">
        <button type="button" class="reset" data-dismiss="modal">Close</button>
        <button type="submit" class="submit" name="login">Login</button>
      </div>

      </form>
    </div>
  </div>
  </div>


  <div id="regis" class="modal fade" role="dialog">
  <div class="modal-dialog">
  <div class="modal-content">
    
    <div class="modal-header">
      <h3> Register </h3>
      <button type="button" class="close" data-dismiss="modal">&times;</button>
    </div>

    <div class="modal-body">
      <form action = "crud.php" method="POST" >
        <label>Email Address</label>
        <input type="email" id="email" name="email" placeholder="Enter Email" required>
        <label>Username</label>
        <input type="text" id="uname" name="uname" placeholder="Enter Username" required>
        <label>Password</label>
        <input type="password" id="pass" name="pass" placeholder="Password" required>
        <label>Confirm Password</label>
        <input type="password" id="cpass" name="cpass" placeholder="Confirm Password" required>
    </div>
      
    <div class="klik">
      <button type="button" class="reset" data-dismiss="modal">Close</button>
      <button type="submit" class="submit" name="submit">Register</button>
    </div>
      
      </form>
  </div>
  </div>
  </div>

<br>
<hr>
<br>

<form action="crud.php" method="GET">
  <div class="atas" style="margin-left:400px; width:870px; background : url('https://gopc.olympus-global.com/photos/hd.php?cd=391279&yr=2017');">
      <br>
      <h1>Hello Coders</h1>
      <p>Welcome to our store, please take a look for the products you might buy.</p>
  </div>

<br>
  <div class="card-deck" style="width: 900px; margin-left:386px;">
  
  <div class="card">
    <img src="web.png" class="card-img-top">
  <div class="card-body">
      <h5 class="card-title">Learning Basic Web Programming</h5>
      <h6>Rp 210.000,-</h6>
      <p class="card-text">Want to be able to make a website? Learn basic components such as HTML, CSS and JavaScript in this class curriculum.</p>
  </div>
  <div class="card-footer">
      <input type="submit" class="button" value="Buy" name="buy1" align="center"> 
  </div>
  </div>

  <div class="card">
    <img src="java.png" class="card-img-top">
  <div class="card-body">
      <h5 class="card-title">Starting Programming in Java</h5>
      <h6>Rp. 150.000,-</h6>
      <p class="card-text"> Learn Java Language for you who want to learn the most popular Object-Oriented Programming (PBO) concepts for developing applications. </p>
  </div>
  <div class="card-footer">
    <input type="submit" class="button" value="Buy" name="buy2" align="center"> 
  </div>
  </div>
  
  <div class="card">
    <img src="phyton.png" class="card-img-top">
  <div class="card-body">
      <h5 class="card-title">Starting Programming in Phyton</h5>
      <h6>Rp. 200.000,-</h6>
      <p class="card-text"> Learn Phython - Fundamental various current industry trends: Data Science, Machine Learning, Infrastructur-management. </p>
  </div>
  <div class="card-footer">
    <input type="submit" class="button" value="Buy" name="buy3" align="center">  
  </div>
  </div>

</div>
</form>
<br>

<h5 style="text-align:center;">© EAD STORE </h5>

</body>
</html>