<?php
session_start();
if(!(isset($_SESSION["login"]))){
    echo "<script>window.location.href='Home.php'</script>";
        exit;
}
include_once("config.php");

$result = mysqli_query($conn, "SELECT*FROM cart_table");
?>

<!DOCTYPE html>
<html>

<head>
    <title>Profile</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="style.css" rel="stylesheet" type="text/css">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
</head>

<body style="background : url('https://www.fg-a.com/wallpapers/white-marble-1-2018.jpg'); background-size:cover;">

<nav class="navbar navbar-light bg-light">
        <a class="navbar-brand" href="Home.php">
            <img src="ead.png" height="40px" class="d-inline-block align-top">
        </a>
        <div style="float: right; margin-right: 40px;">
            <table>
                <tr>
                    <td><a href="cart.php"><img src="cart.png" alt="Cart" width="20px"></a></td>
                    <td>
                        <div class="bs-example" style="padding: 5px;">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?=$_SESSION["user"]?></a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a href="profil.php" class="dropdown-item">Profile</a>
                                <div class="dropdown-divider"></div>
                                <a href="logout.php" class="dropdown-item">Logout</a>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </nav>
    <br><br>
    <h4 style="text-align:center;"> Profile </h4> <br>
    <form action="crud.php" method="POST">
    <center>  <table>  </center>
        <tr height = "50px">
            <td><label> Email </label></td>
            <td>&emsp;&emsp;&emsp;&emsp;</td>
            <td><label readonly class="form-control-plaintext"><?=$_SESSION['email']?></label></td>
        </tr>
 
        <tr height = "50px">
            <td><label> Username </label></td>
            <td></td>
            <td><input type="text" name="uuser" class="form-control" placeholder="Username" value="<?=$_SESSION['user']?>"></td>
        </tr>

        <tr height = "50px">
            <td><label> Mobile Number </label></td>
            <td></td>
            <td><input type="number" name="uphone" class="form-control" placeholder="Mobile Number" value="<?=$_SESSION['hp']?>"></td>
        </tr>

        <tr height = "50px">
            <td><label> New Password </label></td>
            <td></td>
            <td><input type="password" name="upass" class="form-control" placeholder="New Password"></td>
        </tr>

        <tr height = "50px">
            <td><label> Confirm Password </label></td>
            <td></td>
            <td><input type="password" name="upassc" class="form-control" placeholder="Confirm Password"></td>
        </tr>

    </table>
            <div>
                <br>
            <button type="submit" class="submit" name="update" style="width: 30%;" >Save</button> <br>
            <a href=Home.php type="button" class="reset" style="width:30%;">Cancel</a>
            </div>
    </form>
  
    <br>

<h6 style="text-align:center;">© EAD STORE </h6>
</body>

</html>