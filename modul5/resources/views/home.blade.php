@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        @foreach($s as $data)
            <div class="card">

                <div class="card-header">
                <img src="{{asset($data->avatar)}}" class="rounded-circle" alt="hehe" width="5%" height="40%">&ensp;
                {{$data->name}}
                </div>

                <div class="card-body">   
                <img src="{{asset($data->image)}}" class="card-img-too" alt="hehehe" width="690" height="450"> <br>
                {{$data->email}} <br>
                {{$data->caption}} 
                @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection