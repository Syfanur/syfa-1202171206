<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert([
            'name' => 'Syfanr',
            'email' => 'syfanur31@gmail.com',
            'password' => bcrypt('syfa31082000'),
            'avatar' => 'img/foto.jpg',
        ]);
    }
}
