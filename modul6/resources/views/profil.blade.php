@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-2">
    <br><br>
      <img class="rounded-circle" src="{{$user->avatar}}" alt="" height="200px" width="200px">
    </div>
    <div class="col-8" >
    <br><br>
      <h2 style="margin-left:80px;" >{{ $user->name }}</h2>
      <a href="post.create" style="margin-left:500px;">Add New Post</a> <br>
      <a href="editprofil" style="margin-left:80px;">Edit Profile</a>
      <p style="margin-left:80px;">{{ $user->posts->count() }} Post</p>
      <p style="margin-left:80px;" ><b> {{ $user->title }} </b>, <br> {{ $user->desctription }}, <br> 
      <a href="www.instagram.com/syfanr/">{{ $user->url }}</a> </p>
    </div>
</div>
    
    <br> <br>
    <div class="row justify-content-left mb-5">
@foreach($user->posts as $post)

    <div class="col-md-4">
    <a href="/post/{{ $post->id }}">
        <img src="{{ $post->image}}" alt="foto" height="250px" width="370px"> <br> </a>
    </div>
    @endforeach
  </div>
</div>
</div>
@endsection
