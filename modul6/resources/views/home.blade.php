@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        @foreach($posts as $data)
            <div class="card">
                <div class="card-header">
                <a href="profil">
                <img src="{{$data->users->avatar}}" class="rounded-circle" alt="hehe" width="5%" height="40%">&ensp;
                </a>
                &ensp;{{$data->users->name}}
                </div>

                <div class="card-body">
                <a href="/post/{{ $data->id }}">
                <img src="{{$data->image}}" class="card-img-too" alt="hehehe" width="690" height="450"> <br>
        </a></div>

        <div class="card-footer">
            <div class="emoticon">
            <form action="/likes" method="post" style="display:inline">
              @csrf
              <button type="submit" name="button_likes" class="btn" value="{{ $data->id }}"><i class="fa fa-heart" style="color:red"></i></button>
                </form>
            <button type="button" name="button" class="btn"><i class="fa fa-comment"></i></button>
            </div>

            <p style="margin-left:10px;">{{ $data->likes }} Likes</p>
            
            <b> {{$data->users->email}} </b>
                {{$data->caption}} <br>
            <p> ---------------------------------------------------------------------------------------------------------------- </p>
            
          @foreach($data->komentar_posts as $komen)
          <p><b>{{ $komen->users->email }}</b> {{ $komen->text }}</p>
          @endforeach
          
          <form action="/tambah_komen" method="post">
            @csrf
            <div class="input-group mb-3">
              <input type="text" class="form-control" placeholder="Add a comment" aria-describedby="basic-addon2" name="komen">
              <div class="input-group-append">
                <button class="btn btn-outline-secondary" type="submit" name="button_komen" value="{{ $data->id }}">Post</button>
              </div>
              
            </div>
          </form>
      </div>

                @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                </div>

          
            <br> <br>
            @endforeach
        </div>
    </div>
</div>
@endsection