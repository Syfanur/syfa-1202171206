@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-8">
     <img src="{{ $posts->image }}">
    </div>
    <div class="col-4">
      <img src="{{$posts->users->avatar}}">
      <br>
      {{ $posts->users->name }}
       <hr><hr>
      <div class="detail">
        <p>
          <b>{{ $posts->users->email }}</b> {{ $posts->caption }}
        </p>

        @foreach($posts->komentar_posts as $komen)
          <p><b>{{ $komen->users->email }}</b> {{ $komen->text }}</p>
          @endforeach

      </div>
      <hr>
      <div class="emoticon">
        <form action="/likes" method="post" style="display:inline">
          @csrf
          <button type="submit" name="button_likes" class="btn" value="{{ $posts->id }}"><i class="fa fa-heart" style="color:red;"></i></button>
        </form>
        <button type="button" name="button" class="btn"><i class="fa fa-comment"></i></button>
      </div>
      <p style="margin-left:12px;"><b>{{ $posts->likes }} Likes</b></p>
      <form action="/tambah_komen" method="post">
        @csrf
        <div class="input-group mb-3">
          <input type="text" class="form-control" placeholder="Add a comment" name="komentar">
          <div class="input-group-append">
            <button class="btn" type="submit" name="button_komen" value="{{ $posts->id }}"></button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection
