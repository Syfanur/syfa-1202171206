<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');



Route::get('/profil', function () {
    return view('profil');
});

Auth::routes();

Route::get('/profil', 'ProfilController@index')->name('profil');

Route::get('/editprofil', function () {
    return view('editprofil');
});

Auth::routes();

Route::get('/editprofil', 'editprofilController@index')->name('editprofil');


// --------------

Route::get('post.create', 'HomeController@create')->name('post.create');
Route::post('post.store', 'HomeController@store')->name('post.store');

// ----
Route::get('profil.update', 'HomeController@update')->name('profil.update');
Route::post('profil.save', 'HomeController@save')->name('profil.save');

// -------
Route::get('/post/{id}', 'HomeController@detail')->name('detailpost');

// --------

Route::post('/likes', 'HomeController@likes')->name('likes');

Route::post('/tambah_komen', 'HomeController@komentar')->name('komen');