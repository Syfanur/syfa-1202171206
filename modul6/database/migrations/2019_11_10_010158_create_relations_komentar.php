<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRelationsKomentar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('komentar_posts', function (Blueprint $table) {
        $table->foreign('user_id')->references('id')->on('users');
        $table->foreign('post_id')->references('id')->on('posts');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('komentar_posts', function (Blueprint $table) {
        $table->dropForeign('komentar_posts_user_id_foreign');
        $table->dropForeign('komentar_posts_post_id_foreign');
      });
    }
}
