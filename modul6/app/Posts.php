<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
    protected $table = "posts";
    protected $fillable = ['caption', 'image'];

    public function users(){
      return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function komentar_posts(){
      return $this->hasMany('App\Komentar_posts', 'post_id', 'id');
    }

}


