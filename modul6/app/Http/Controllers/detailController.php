<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Posts as Posts;
use App\Komentar_posts as Komentar_posts;
use Auth;
use App\User as User;

class detailController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        // $post = DB::table('posts')
        // ->join('users', 'posts.user_id', '=', 'users.id')
        // ->select('posts.*', 'users.*') -> get();
        // return view('profil',
        // [ 's'=> $post
        // ]);

        $id = Auth::user()->id;
        $user = User::find($id);
        $post = Post::find($id);

        return view('detailpost', compact('user', 'post'));
    }
}