<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Posts as Posts;
use App\Komentar_posts as Komentar_posts;
use Auth;
use App\User as User;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $data = Posts::with('users')->with('komentar_posts')->get();
        return view('home', ['posts' => $data] );

    }

    public function likes(Request $request)
  {
    $id = $request->button_likes;

    $likes = Posts::find($id)->increment('likes');

    return redirect('/home');
  }

  public function komentar(Request $request)
  {
    Komentar_posts::insert([
      'user_id' => Auth::user()->id,
      'post_id' => $request->button_komen,
      'text' => $request->komen,
    ]);

    return redirect('home');
    }

  public function create(){
    return view('post');
  }

  public function store(){
    DB::table('posts')->insert([
      'user_id' => Auth::user()->id,
      'image' => 'img/post4.jpg',
      'caption' => request('caption'),
      
  ]);
  
    return redirect('home');
  }

  public function update(){
    $user = User::find($id);
   return view('editprofil', ['user' => $user]);
  }

  public function save(){
    DB::table('users')->update([
      'title' => request('title'),
      'desctription' => request('desctription'),
      'url' => request('url'),
    ]);
    return redirect('/profil');
  }


  public function detail($id){

  $user = new User();
  $user = User::find($id);
  $data = Posts::with('users')
    ->with('komentar_posts')
    ->where('id', $id)
    ->get();

    //return $data[0]->users->name;

    return view('detailpost', ['posts' => $data[0]], compact('user'));

  }}
        