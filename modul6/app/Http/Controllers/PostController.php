<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Posts;
use Auth;

class PostController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
  
        
    
        

        public function addpost(Request $request){
          $add = new Posts();
          try{
            $add -> caption = $request->input('caption');
            $file = $request -> file('image');
            $file_name = $file -> getClientOriginalName();
            $path = public_path("/img");
            $file ->move($path, $file_name);
            $add->foto = $file_name;
        
            $add = submit();
          } catch (Exception $exception) {
              return redirect('/home')->with('alert', 'Add post belum berhasil');
          }
        
          return redirect('/home')->with('success', 'Post berhasil ditambahkan');
        }

      }