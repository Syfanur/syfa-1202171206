<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User as User;
use App\Posts as Post;
use Auth;

class ProfilController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // $post = DB::table('posts')
        // ->join('users', 'posts.user_id', '=', 'users.id')
        // ->select('posts.*', 'users.*') -> get();
        // return view('profil',
        // [ 's'=> $post
        // ]);

        $id = Auth::user()->id;
        $user = User::find($id);
        $post = Post::find($id);

        return view('profil', compact('user', 'post'));
    }
}